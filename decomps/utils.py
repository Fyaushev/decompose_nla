import copy


def calculate_layer_cr(model_stats, lnames_to_compress, cr=2):
    flops_to_compress = 0

    for lname in lnames_to_compress:
        flops_to_compress += model_stats.flops[lname][0]
    uncompressed_flops = model_stats.total_flops - flops_to_compress
    layer_cr = flops_to_compress * cr / (flops_to_compress + uncompressed_flops * (1 - cr))
    return layer_cr


def get_layer_by_name(model, mname):
    '''
    Extract layer using layer name
    '''
    module = model
    mname_list = mname.split('.')
    for mname in mname_list:
        module = module._modules[mname]

    return module


def replace_conv_layer_by_name(model, mname, new_layer):
    '''
    Replace layer using layer name
    '''
    module = model
    mname_list = mname.split('.')
    for mname in mname_list[:-1]:
        module = module._modules[mname]
    module._modules[mname_list[-1]] = new_layer


def cr_to_svd_rank(layer, decomposition='spatial-svd', cr=2.):
    weight_shape = layer.weight.shape
    cout, cin, kh, kw = weight_shape

    initial_count = cout * cin * kh * kw

    if decomposition == 'spatial-svd':
        rank = initial_count // (cr * (cin * kh + kw * cout))
    elif decomposition == 'weight-svd':
        rank = initial_count // (cr * (cin * kh * kw + cout))
    else:
        print('Wrong decomposiiton name. Should be spatial-svd or weight-svd')
        rank = None

    return int(rank)

