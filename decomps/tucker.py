import copy
import types
from collections import defaultdict

import numpy as np
import torch
from sktensor import dtensor, tucker
from torch import nn


class TuckerLayer():
    def __init__(self, layer, layer_name, ranks=None,
                 *args, **kwargs):

        self.layer_name = layer_name
        self.layer = layer

        if isinstance(self.layer, nn.Sequential):
            self.cin = self.layer[0].in_channels
            self.cout = self.layer[2].out_channels

            self.k_size = self.layer[1].kernel_size
            self.p = self.layer[1].padding
            self.s = self.layer[1].stride

            weight = self.layer[1].weight.data
            self.weight = weight.reshape(*weight.shape[:2], -1)
            self.bias = None if self.layer[2].bias is None else self.layer[2].bias.data
        elif isinstance(self.layer, nn.Conv2d):
            self.cin = self.layer.in_channels
            self.cout = self.layer.out_channels

            self.k_size = self.layer.kernel_size
            self.p = self.layer.padding
            self.s = self.layer.stride

            weight = self.layer.weight.data
            self.weight = weight.reshape(*weight.shape[:2], -1)
            self.bias = None if self.layer.bias is None else self.layer.bias.data
        else:
            raise AttributeError('unsuitable type', type(self.layer))

        if isinstance(ranks, types.FunctionType):
            self.ranks = ranks(self.weight, *args, **kwargs)
        elif isinstance(ranks, list) or isinstance(ranks, tuple):
            self.ranks = ranks
        else:
            raise AttributeError('unsuitable type', type(ranks))

        self.new_layers = nn.Sequential()

        for j, l in enumerate(self.get_new_layers()):
            self.new_layers.add_module(f'{self.layer_name}-{j}', l)

        weights, biases = self.get_tucker_factors()

        for j, (w, b) in enumerate(zip(weights, biases)):
            self.new_layers.__getattr__(f'{self.layer_name}-{j}').weight.data = w
            if b is not None:
                self.new_layers.__getattr__(f'{self.layer_name}-{j}').bias.data = b
            else:
                self.new_layers.__getattr__(f'{self.layer_name}-{j}').bias = None

    def get_new_layers(self):
        layers = [nn.Conv2d(in_channels=self.cin,
                            out_channels=self.ranks[1],
                            kernel_size=(1, 1)),
                  nn.Conv2d(in_channels=self.ranks[1],
                            out_channels=self.ranks[0],
                            kernel_size=self.k_size,
                            groups=1, padding=self.p, stride=self.s),
                  nn.Conv2d(in_channels=self.ranks[0],
                            out_channels=self.cout,
                            kernel_size=(1, 1))]
        return layers

    def get_tucker_factors(self):

        weights = dtensor(self.weight.cpu())
        if self.bias is not None:
            bias = self.bias.cpu()
        else:
            bias = self.bias

        core, (U_cout, U_cin, U_dd) = tucker.hooi(weights,
                                                  [self.ranks[0],
                                                   self.ranks[1],
                                                   weights.shape[-1]],
                                                  init='nvecs')
        core = core.dot(U_dd.T)

        w_cin = np.array(U_cin)
        w_core = np.array(core)
        w_cout = np.array(U_cout)

        if isinstance(self.layer, nn.Sequential):
            w_cin_old = self.layer[0].weight.cpu().data
            w_cout_old = self.layer[2].weight.cpu().data

            U_cin_old = np.array(torch.transpose(w_cin_old.reshape(w_cin_old.shape[:2]), 1, 0))
            U_cout_old = np.array(w_cout_old.reshape(w_cout_old.shape[:2]))

            w_cin = U_cin_old.dot(U_cin)
            w_cout = U_cout_old.dot(U_cout)

        w_cin = torch.FloatTensor(np.reshape(w_cin.T, [self.ranks[1], self.cin, 1, 1])).contiguous()
        w_core = torch.FloatTensor(np.reshape(w_core, [self.ranks[0], self.ranks[1], *self.k_size])).contiguous()
        w_cout = torch.FloatTensor(np.reshape(w_cout, [self.cout, self.ranks[0], 1, 1])).contiguous()

        return [w_cin, w_core, w_cout], [None, None, bias]



def initialize_ranks(ranks, lnames):
    ret_ranks = {k: 0 for k in lnames}

    if ranks is not None:
        ret_ranks = {k: v for k, v in ranks.items() if k in lnames}

    return ret_ranks


def get_compressed_model(model, layer_names, ranks,
                         return_ranks=False, *args, **kwarg):
    compressed_model = copy.deepcopy(model)
    new_ranks = defaultdict()

    for lname in layer_names:
        rank = ranks[lname]

        if rank is not None:
            subm_names = lname.strip().split('.')
            layer = compressed_model.__getattr__(subm_names[0])
            for s in subm_names[1:]:
                layer = layer.__getattr__(s)

            decomposed_layer = TuckerLayer(layer, subm_names[-1], rank, *args, **kwarg)

            new_ranks[lname] = decomposed_layer.ranks

            if len(subm_names) > 1:
                m = compressed_model.__getattr__(subm_names[0])
                for s in subm_names[1:-1]:
                    m = m.__getattr__(s)
                m.__setattr__(subm_names[-1], decomposed_layer.new_layers)
            else:
                compressed_model.__setattr__(subm_names[-1], decomposed_layer.new_layers)
        else:
            return None

    if return_ranks:
        return compressed_model, new_ranks
    else:
        return compressed_model
