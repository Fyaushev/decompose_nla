import numpy as np
import torch
from torch import nn
    
    
class SVD_Spatial_conv_layer(torch.nn.Module):
    def __init__(self, layer, rank=None):
        super(SVD_Spatial_conv_layer, self).__init__()
        
        self.c_in = layer.in_channels
        self.c_out = layer.out_channels
        self.padding = layer.padding
        self.stride = layer.stride
        self.kernel_size = layer.kernel_size
        self.h = layer.kernel_size[0]
        self.w = layer.kernel_size[1]
        self.is_bias = layer.bias is not None 
        if self.is_bias:
            self.bias = layer.bias

        self.rank = rank
        self.svd_decomposition = self.__replace__(layer)
        
    def __replace__(self, layer):   
        
        weight_reshaped = layer.weight.permute(0, 2, 1, 3).reshape(self.c_out * self.h, 
                                                                   self.c_in * self.w).cpu().data.numpy()
        U, S, Vt = np.linalg.svd(weight_reshaped, full_matrices=False)
            
        w0 = np.dot(np.diag(np.sqrt(S[0:self.rank])),Vt[0:self.rank, :])
        w1 = np.dot(U[:, 0:self.rank], np.diag(np.sqrt(S[0:self.rank])))

        new_layers = [
            nn.Conv2d(in_channels=self.c_in, out_channels=self.rank,
                      kernel_size=(1, self.w), bias=False, 
                      stride=(1, self.stride[1]),
                      padding=(0, self.padding[1])),
            
            nn.Conv2d(in_channels=self.rank, out_channels=self.c_out,
                      kernel_size=(self.h, 1), bias=self.is_bias, 
                      stride=(self.stride[0], 1),
                      padding=(self.padding[0], 0))]

        new_kernels = [torch.FloatTensor(w0).reshape(self.rank, self.c_in, 1, self.w),
                       torch.FloatTensor(w1).reshape(self.c_out, self.h, self.rank, 1).permute(0, 2, 1, 3)]
        
        with torch.no_grad():
            for i in range(len(new_kernels)):
                new_layers[i].weight = nn.Parameter(new_kernels[i].cpu())
                if i == len(new_kernels)-1 and self.is_bias:
                    new_layers[i].bias = nn.Parameter(self.bias)
        
        return nn.Sequential(*new_layers)
    
    def forward(self, x):
        out = self.svd_decomposition(x)
        return out
    
    
class SVD_Weight_conv_layer(torch.nn.Module):
    def __init__(self, layer, rank=None):
        super(SVD_Weight_conv_layer, self).__init__()
        
        self.c_in = layer.in_channels
        self.c_out = layer.out_channels
        self.padding = layer.padding
        self.stride = layer.stride
        self.kernel_size = layer.kernel_size
        self.h = layer.kernel_size[0]
        self.w = layer.kernel_size[1]
        self.is_bias = layer.bias is not None 
        if self.is_bias:
            self.bias = layer.bias

        self.rank = rank                
        self.svd_decomposition = self.__replace__(layer)
        
    def __replace__(self, layer):
        
        weight_reshaped = layer.weight.reshape(self.c_out, self.c_in * self.h * self.w).cpu().data.numpy()
        U, S, Vt = np.linalg.svd(weight_reshaped, full_matrices=False)
            
        w0 = np.dot(np.diag(np.sqrt(S[0:self.rank])),Vt[0:self.rank, :])
        w1 = np.dot(U[:, 0:self.rank], np.diag(np.sqrt(S[0:self.rank])))

        new_layers = [
            nn.Conv2d(in_channels=self.c_in, out_channels=self.rank,
                      kernel_size=(self.h, self.w), bias=False, 
                      stride=layer.stride,
                      padding=layer.padding),
            nn.Conv2d(in_channels=self.rank, out_channels=self.c_out,
                      kernel_size=(1, 1), bias=self.is_bias, 
                      stride=(1, 1),
                      padding=0)
        ]

        new_kernels = [torch.FloatTensor(w0).reshape(self.rank, self.c_in, self.h, self.w),
                       torch.FloatTensor(w1).reshape(self.c_out, self.rank, 1, 1)]
        
        with torch.no_grad():
            for i in range(len(new_kernels)):
                new_layers[i].weight = nn.Parameter(new_kernels[i].cpu())
                if i == len(new_kernels)-1 and self.is_bias:
                    new_layers[i].bias = nn.Parameter(self.bias)
        
        return nn.Sequential(*new_layers)
    
    def forward(self, x):
        out = self.svd_decomposition(x)
        return out
   