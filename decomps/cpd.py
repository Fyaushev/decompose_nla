import torch
import torch.nn as nn
import tensorly as tl
from tensorly.decomposition import parafac


tl.set_backend('pytorch')

class CP_block(torch.nn.Module):

    def __init__(self, layer, rank):
        super(CP_block, self).__init__()
        self.rank = rank
        self.padding = layer.padding
        self.stride = layer.stride
        self.is_bias = layer.bias is not None 
        if self.is_bias:
            self.bias = layer.bias

        cp_decomposition = self._cp_decomposition_conv_layer(layer)

        self.cp_decomposition = cp_decomposition
        
        
    def _cp_decomposition_conv_layer(self, layer):
        decomp = parafac(layer.weight.data, rank=self.rank, init='svd')
        last, first, vertical, horizontal = decomp.factors[0], decomp.factors[1], \
                                            decomp.factors[2], decomp.factors[3]
        
        pointwise_s_to_r_layer = nn.Conv2d(in_channels=first.shape[0], \
                 out_channels=first.shape[1], kernel_size=(1,1), stride=1,
                 padding=0, dilation=layer.dilation, bias=False)

        depthwise_vertical_layer = torch.nn.Conv2d(in_channels=vertical.shape[1], 
                out_channels=vertical.shape[1], kernel_size=(vertical.shape[0], 1),
                stride=1, padding=(self.padding[0], 0), dilation=layer.dilation,
                groups=vertical.shape[1], bias=False)

        depthwise_horizontal_layer = \
            nn.Conv2d(in_channels=horizontal.shape[1], \
                out_channels=horizontal.shape[1], 
                kernel_size=(1, horizontal.shape[0]), stride=self.stride,
                padding=(0, self.padding[0]), 
                dilation=layer.dilation, groups=horizontal.shape[1], bias=False)

        pointwise_r_to_t_layer = nn.Conv2d(in_channels=last.shape[1], \
                out_channels=last.shape[0], kernel_size=1, stride=1,
                padding=0, dilation=layer.dilation, bias=True)
        
        if self.is_bias:
            pointwise_r_to_t_layer.bias.data = layer.bias.data

        depthwise_horizontal_layer.weight.data = \
            torch.transpose(horizontal, 1, 0).unsqueeze(1).unsqueeze(1)
        depthwise_vertical_layer.weight.data = \
            torch.transpose(vertical, 1, 0).unsqueeze(1).unsqueeze(-1)
        pointwise_s_to_r_layer.weight.data = \
            torch.transpose(first, 1, 0).unsqueeze(-1).unsqueeze(-1)
        pointwise_r_to_t_layer.weight.data = last.unsqueeze(-1).unsqueeze(-1)

        new_layers = [pointwise_s_to_r_layer, depthwise_vertical_layer, \
                        depthwise_horizontal_layer, pointwise_r_to_t_layer]
    
        return nn.Sequential(*new_layers)

    def forward(self, x):
     
        out = self.cp_decomposition(x)
        return out