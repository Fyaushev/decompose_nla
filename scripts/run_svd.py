import argparse
import os
from pathlib import Path

import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from flopco import FlopCo
from musco.pytorch import CompressorPR
from decomps.models import ResNet18
from decomps.test import get_accuracy
from decomps.utils import replace_conv_layer_by_name, calculate_layer_cr, get_layer_by_name
import copy
from decomps.svd import SVD_Spatial_conv_layer, SVD_Weight_conv_layer


def cr_to_svd_rank(layer, decomposition='spatial-svd', cr=2.):
    weight_shape = layer.weight.shape
    cout, cin, kh, kw = weight_shape

    initial_count = cout * cin * kh * kw
    if decomposition == 'spatial-svd':
        rank = initial_count // (cr * (cin * kh + kw * cout))
    elif decomposition == 'weight-svd':
        rank = initial_count // (cr * (cin * kh * kw + cout))
    else:
        print('Wrong decomposiiton name. Should be spatial-svd or weight-svd')
        rank = None

    return int(rank)


parser = argparse.ArgumentParser()
parser.add_argument('--model_path')
parser.add_argument('--save_path')

args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'

model = ResNet18().to(device)
checkpoint = torch.load(args.model_path, map_location=torch.device(device))

model.load_state_dict(checkpoint['net'])
model.eval()

model_stats = FlopCo(model, img_size = (1, 3, 32, 32), device=device)
lnames_to_compress = [lname for lname, _ in model.named_modules() if 'conv' in lname]
lnames_to_compress = lnames_to_compress[1:]

save_folder = Path(args.save_path) / 'weight_svd'
save_folder.mkdir(parents=True)

for cr in [2, 4, 8]:
    net = copy.deepcopy(model)
    layer_cr = calculate_layer_cr(model_stats, lnames_to_compress, cr=cr)

    for lname in lnames_to_compress:
        layer = get_layer_by_name(net, lname)
        r = cr_to_svd_rank(layer, decomposition='weight-svd', cr=layer_cr)
        compressed_layer = SVD_Weight_conv_layer(layer, rank = r)
        replace_conv_layer_by_name(net, lname, compressed_layer)

        param_reduction_rates = {lname: layer_cr for lname in lnames_to_compress}

    torch.save(net, save_folder / f'cr{cr}.pth')

    print(f'cr={cr}: {get_accuracy(net, device)}')


