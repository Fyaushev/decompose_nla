import argparse
import os
from pathlib import Path

import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from flopco import FlopCo
from musco.pytorch import CompressorPR
from decomps.models import ResNet18
from decomps.test import get_accuracy
from decomps.utils import calculate_layer_cr

parser = argparse.ArgumentParser()
parser.add_argument('--model_path')
parser.add_argument('--save_path')

args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'

model = ResNet18()
checkpoint = torch.load(args.model_path, map_location=torch.device('cpu'))

model.load_state_dict(checkpoint['net'])
model.eval()

model_stats = FlopCo(model, img_size = (1, 3, 32, 32), device='cpu')
lnames_to_compress = [lname for lname, _ in model.named_modules() if 'conv' in lname]
lnames_to_compress = lnames_to_compress[1:]


save_folder = Path(args.save_path) / 'tucker'
save_folder.mkdir(parents=True)

for cr in [2, 4, 8]:
    layer_cr = calculate_layer_cr(model_stats, lnames_to_compress, cr=cr)

    param_reduction_rates = {lname: layer_cr for lname in lnames_to_compress}

    compressor = CompressorPR(model,
                              model_stats,
                              conv2d_nn_decomposition='tucker2',
                              ft_every=len(lnames_to_compress),
                              param_reduction_rates=param_reduction_rates,
                              nglobal_compress_iters=1)

    compressor.lnames = lnames_to_compress
    compressor.compression_step()
    compressed_model = compressor.compressed_model

    torch.save(compressed_model, save_folder / f'cr{cr}.pth')
    print(f'cr={cr}: {get_accuracy(compressed_model, device)}')


