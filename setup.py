import runpy
from pathlib import Path

from setuptools import setup, find_packages

name = 'decomps'
root = Path(__file__).parent
with open(root / 'requirements.txt', encoding='utf-8') as file:
    requirements = file.read().splitlines()

setup(
    name=name,
    packages=find_packages(include=(name,)),
    include_package_data=True,
    install_requires=requirements,
    python_requires='>=3.6',
)
